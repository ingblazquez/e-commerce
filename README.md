# Instrucciones

Para correr la aplicacion debera:

- Clonar el repositorio de forma local.
- Ejecutar **npm install** dentro del directorio root
- Luego que termine de instalar las dependencias debera ejecutar **npm start**, para inicializar la aplicacion

# Rutas

- **Pagina de inicio:** http://localhost:3000/
- **Pagina de checkout:** http://localhost:3000/checkout
