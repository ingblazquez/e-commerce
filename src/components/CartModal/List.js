import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector, useDispatch } from "react-redux";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import Divider from "@material-ui/core/Divider";
import actions from "../../redux/cart/cart.actions";

const useStyles = makeStyles(theme => ({
 root: {
  width: "100%"
 }
}));

export default function CartList() {
 const classes = useStyles();
 const dispatch = useDispatch();
 const cartItems = useSelector(state => state.cart.cartItems);
 const cartTotal = useSelector(state =>
  state.cart.cartItems.reduce(
   (accumulatedQuantity, cartItem) =>
    accumulatedQuantity + cartItem.quantity * cartItem.price,
   0
  )
 );

 const addItem = (e, item) => {
  e.preventDefault();
  dispatch(actions.addItem(item));
 };
 const removeItem = (e, item) => {
  e.preventDefault();
  dispatch(actions.removeItem(item));
 };
 if (cartItems.length < 1) return <p>Your cart is empty...</p>;

 return (
  <div className={classes.demo}>
   <List dense className={classes.root}>
    {cartItems.map((item, idx) => {
     return (
      <ListItem key={idx}>
       <ListItemText
        primary={item.title}
        secondary={
         <React.Fragment>
          {item.quantity} x {item.price}
         </React.Fragment>
        }
        className={classes.textMargin}
       />
       <ListItemSecondaryAction>
        <IconButton
         aria-label="add"
         size="small"
         onClick={e => addItem(e, item)}
        >
         <AddIcon />
        </IconButton>
        <IconButton
         aria-label="remove"
         size="small"
         onClick={e => removeItem(e, item)}
        >
         <RemoveIcon />
        </IconButton>
       </ListItemSecondaryAction>
      </ListItem>
     );
    })}
   </List>
   <Divider />
   <List dense className={classes.root}>
    <ListItem>
     <ListItemText
      primary={<React.Fragment>Total $ {cartTotal}</React.Fragment>}
     />
    </ListItem>
   </List>
  </div>
 );
}
