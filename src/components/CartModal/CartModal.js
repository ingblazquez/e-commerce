import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Button from "../../components/CustomButtons/Button.js";
import List from "./List";
import { useSelector, useDispatch } from "react-redux";
import actions from "../../redux/cart/cart.actions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";

const useStyles = makeStyles(theme => ({
 dialogContentContainer: {
  minWidth: "390px",
  backgroundColor: theme.palette.background.paper
 },
 dialogContent: {
  overflowX: "scroll"
 },
 paper: {
  width: "80%",
  maxHeight: 435
 }
}));

export default function CartModal(props) {
 const classes = useStyles();
 const dispatch = useDispatch();
 const hidden = useSelector(state => state.cart.hidden);

 const handleClose = e => {
  e.preventDefault();
  dispatch(actions.toggleCartHidden());
 };

 const handleCheckout = () => {
  dispatch(actions.toggleCartHidden());
 };

 return (
  //   <div className={classes.root}>
  <Dialog
   maxWidth="xs"
   aria-labelledby="confirmation-dialog-title"
   open={!hidden}
   onClose={handleClose}
   classes={{
    paper: classes.paper
   }}
  >
   <DialogTitle id="confirmation-dialog-title">Shopping Cart</DialogTitle>

   <DialogContent dividers>
    <div className={classes.dialogContentContainer}>
     <div className={classes.dialogContent}>
      <List />
     </div>
    </div>
   </DialogContent>
   <DialogActions>
    <Link to="/checkout">
     <Button onClick={handleCheckout} variant="contained">
      CHECKOUT
     </Button>
    </Link>
   </DialogActions>
  </Dialog>
  //   </div>
 );
}
