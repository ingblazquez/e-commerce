/*eslint-disable*/
import React from "react";
import { useDispatch } from "react-redux";
// react components for routing our app without refresh

import actions from "../../redux/cart/cart.actions";

import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

import Button from "../../components/CustomButtons/Button.js";

import styles from "../../assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
 const dispatch = useDispatch();
 const classes = useStyles();
 const handleCartClick = e => {
  e.preventDefault();
  dispatch(actions.toggleCartHidden());
 };
 return (
  <List className={classes.list}>
   <ListItem className={classes.listItem}>
    <Tooltip
     id="twitter-tooltip"
     title="Follow us on twitter"
     placement={window.innerWidth > 959 ? "top" : "left"}
     classes={{ tooltip: classes.tooltip }}
    >
     <Button href="#" color="transparent" className={classes.navLink}>
      <i className={classes.socialIcons + " fab fa-twitter"} />
     </Button>
    </Tooltip>
   </ListItem>
   <ListItem className={classes.listItem}>
    <Tooltip
     id="facebook-tooltip"
     title="Follow us on facebook"
     placement={window.innerWidth > 959 ? "top" : "left"}
     classes={{ tooltip: classes.tooltip }}
    >
     <Button color="transparent" href="#" className={classes.navLink}>
      <i className={classes.socialIcons + " fab fa-facebook"} />
     </Button>
    </Tooltip>
   </ListItem>
   <ListItem className={classes.listItem}>
    <Tooltip
     id="cart-tooltip"
     title="Shopping cart"
     placement={window.innerWidth > 959 ? "top" : "left"}
     classes={{ tooltip: classes.tooltip }}
    >
     <Button
      color="transparent"
      className={classes.navLink}
      onClick={e => handleCartClick(e)}
     >
      <i className={classes.socialIcons + " fas fa-shopping-cart"} />
     </Button>
    </Tooltip>
   </ListItem>
  </List>
 );
}
