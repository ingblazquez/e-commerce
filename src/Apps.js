import React from "react";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { persistStore } from "redux-persist";
import { Provider } from "react-redux";
import "./assets/scss/material-kit-react.scss?v=1.8.0";
import HomePage from "./views/HomePage/HomePage.js";
import Checkout from "./views/Checkout/Checkout.js";
import Layout from "./Layout/Layout.js";
import { PersistGate } from "redux-persist/integration/react";
import store from "./redux/store";

var hist = createBrowserHistory();
const persistor = persistStore(store);

export default () => {
 return (
  <Provider store={store}>
   <PersistGate persistor={persistor}>
    <Router history={hist}>
     <Layout>
      <Switch>
       <Route path="/checkout" component={Checkout} />
       <Route path="/" component={HomePage} />
      </Switch>
     </Layout>
    </Router>
   </PersistGate>
  </Provider>
 );
};
