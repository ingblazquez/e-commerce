import React from "react";
import ReactDOM from "react-dom";
import "./assets/scss/material-kit-react.scss?v=1.8.0";

// pages for this product
import Apps from "./Apps";

ReactDOM.render(<Apps />, document.getElementById("root"));
