import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { useSelector } from "react-redux";
import styles from "../../assets/jss/material-kit-react/views/landingPageSections/teamStyle.js";
import Button from "../../components/CustomButtons/Button.js";
import { useDispatch } from "react-redux";
import actions from "../../redux/cart/cart.actions";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
const useStyles = makeStyles(styles);

export default function Checkout() {
 const classes = useStyles();
 const dispatch = useDispatch();
 const cartItems = useSelector(state => state.cart.cartItems);
 const cartTotal = useSelector(state =>
  state.cart.cartItems.reduce(
   (accumulatedQuantity, cartItem) =>
    accumulatedQuantity + cartItem.quantity * cartItem.price,
   0
  )
 );

 const addItem = (e, item) => {
  e.preventDefault();
  dispatch(actions.addItem(item));
 };
 const removeItem = (e, item) => {
  e.preventDefault();
  dispatch(actions.removeItem(item));
 };
 return (
  <div className={classes.section}>
   <h2 className={classes.title}>Checkout</h2>
   <div style={{ overflowX: "auto" }}>
    <Table aria-label="simple table" style={{ minWidth: "600px" }}>
     <TableHead>
      <TableRow>
       <TableCell>Book</TableCell>
       <TableCell align="right">Quantity</TableCell>
       <TableCell align="right">Price</TableCell>
       <TableCell align="right">Add / Remove</TableCell>
      </TableRow>
     </TableHead>
     <TableBody>
      {cartItems.map((item, idx) => (
       <TableRow key={idx}>
        <TableCell>{item.title}</TableCell>
        <TableCell align="right">{item.quantity}</TableCell>
        <TableCell align="right">{item.price}</TableCell>
        <TableCell align="right">
         <IconButton
          aria-label="add"
          size="small"
          onClick={e => addItem(e, item)}
         >
          <AddIcon />
         </IconButton>
         <IconButton
          aria-label="remove"
          size="small"
          onClick={e => removeItem(e, item)}
         >
          <RemoveIcon />
         </IconButton>
        </TableCell>
       </TableRow>
      ))}
      <TableRow>
       <TableCell component="th" align="right" colSpan={2}>
        Total
       </TableCell>
       <TableCell align="right">{cartTotal}</TableCell>
      </TableRow>
     </TableBody>
     <TableFooter>
      <TableRow>
       <TableCell component="th" align="right" colSpan={4}>
        <Button>PLACE ORDER</Button>
       </TableCell>
      </TableRow>
     </TableFooter>
    </Table>
   </div>
  </div>
 );
}
