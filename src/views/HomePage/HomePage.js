import React, { useState } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";
// @material-ui/icons
// import Button from "@material-ui/core/Button";
import matchSorter from "match-sorter";
// core components
import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
import Button from "../../components/CustomButtons/Button.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import CardFooter from "../../components/Card/CardFooter.js";
import { useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import styles from "../../assets/jss/material-kit-react/views/landingPageSections/teamStyle.js";
import booksImage from "../../assets/img/book.jpeg";
//
import { useDispatch } from "react-redux";
import actions from "../../redux/cart/cart.actions";

const useStyles = makeStyles(styles);

export default function HomePage() {
 const classes = useStyles();
 const dispatch = useDispatch();
 const books = useSelector(state => state.cart.books);
 const loading = useSelector(state => state.cart.loading);
 const [controlledBooks, setControlledBooks] = useState([]);

 React.useEffect(() => {
  dispatch(actions.fetchBooks());
 }, [dispatch]);

 React.useEffect(() => {
  setControlledBooks(books);
 }, [books]);

 const handleChange = e => {
  e.preventDefault();
  const filteredBooks = matchSorter(books, e.target.value, {
   keys: ["title", "author"]
  });
  setControlledBooks(filteredBooks);
 };

 const imageClasses = classNames(
  classes.imgRaised,
  classes.imgRoundedCircle,
  classes.imgFluid
 );

 return (
  <div className={classes.section}>
   <h2 className={classes.title}>Find your book</h2>
   {loading ? (
    <div style={{ padding: 20 }}>
     <Skeleton variant="rect" width="100%" height={118} />
     <Skeleton />
     <Skeleton width="60%" />
    </div>
   ) : (
    <React.Fragment>
     <TextField
      id="type-something"
      label="Try title or author..."
      onChange={e => handleChange(e)}
     />
     <div>
      <GridContainer>
       {controlledBooks.map((item, idx) => (
        <GridItem xs={12} sm={12} md={4} key={idx}>
         <Card plain>
          <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
           <img src={booksImage} alt="..." className={imageClasses} />
          </GridItem>
          <h4 className={classes.cardTitle}>
           <a
            style={{ color: "#6c757d" }}
            href={item.link}
            target="_blank"
            rel="noopener noreferrer"
           >
            {item.title}
           </a>
           <br />
           <small className={classes.smallTitle}>
            {item.author} - {item.publishedDate}
           </small>
          </h4>
          <CardBody>
           <p className={classes.description}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            consequat hendrerit laoreet.
           </p>
           <b className={classes.description}>$ {item.price}</b>

           <CardFooter>
            <div style={{ textAlign: "center", margin: "auto" }}>
             <Button
              variant="contained"
              onClick={() => dispatch(actions.addItem(item))}
             >
              ADD TO CART
             </Button>
            </div>
           </CardFooter>
          </CardBody>
         </Card>
        </GridItem>
       ))}
      </GridContainer>
     </div>
    </React.Fragment>
   )}
  </div>
 );
}
