import thunkMiddleware from "redux-thunk";
import logger from "redux-logger";
import { createStore, compose, applyMiddleware } from "redux";
import rootReducer from "./rootReducer";

const composedEnhancers =
 window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [thunkMiddleware, logger];

const store = createStore(
 rootReducer,
 composedEnhancers(applyMiddleware(...middlewares))
);
export default store;
