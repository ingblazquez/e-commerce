import axios from "axios";

import types from "./cart.actionTypes";
const toggleCartHidden = () => ({
 type: types.TOGGLE_CART_HIDDEN
});
const addItem = item => ({
 type: types.ADD_ITEM,
 payload: item
});
const removeItem = item => ({
 type: types.REMOVE_ITEM,
 payload: item
});
const clearItemFromCart = item => ({
 type: types.CLEAR_ITEM_FROM_CART,
 payload: item
});
const setLoading = payload => ({
 type: types.SET_LOADING,
 payload
});

//
const config = {
 headers: {
  Authorization:
   "eyJ0eXAiOiJKV1QiLCJhbiAoOiJIUzI1NiJ9.eyJvX2F1dGhfYXBwbGljYXRpb25faWQiOjF9.h7ZNPpbd_Bu8yL27fmCAI_wgyZJI44b5eRRxwWUcwaQ"
 }
};
const fetchBooks = () => {
 return dispatch => {
  dispatch(setLoading(true));
  axios
   .get(
    "https://private-anon-ee149e5f0c-wbooksapi.apiary-mock.com/api/v1/book_suggestions",
    config
   )
   .then(response => {
    console.log("response.data ", response.data);
    dispatch(setBooks(response.data));
    dispatch(setLoading(false));
   })
   .catch(error => {
    console.log("error: ", error);
    dispatch(setLoading(false));
   });
 };
};
const setBooks = payload => ({
 type: types.SET_BOOKS,
 payload
});

export default {
 toggleCartHidden,
 addItem,
 removeItem,
 clearItemFromCart,
 fetchBooks,
 setBooks,
 setLoading
};
