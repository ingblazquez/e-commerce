import types from "./cart.actionTypes";
import { addItemToCart, removeItemFromCart } from "./cart.utils";

const INITIAL_STATE = {
 hidden: true,
 cartItems: [],
 books: [],
 loading: false
};

const cartReducer = (state = INITIAL_STATE, action) => {
 switch (action.type) {
  case types.TOGGLE_CART_HIDDEN:
   return {
    ...state,
    hidden: !state.hidden
   };
  case types.ADD_ITEM:
   return {
    ...state,
    cartItems: addItemToCart(state.cartItems, action.payload)
   };
  case types.CLEAR_ITEM_FROM_CART:
   return {
    ...state,
    cartItems: state.cartItems.filter(
     cartItem => cartItem.id !== action.payload.id
    )
   };
  case types.REMOVE_ITEM:
   return {
    ...state,
    cartItems: removeItemFromCart(state.cartItems, action.payload)
   };

  case types.SET_BOOKS:
   return {
    ...state,
    books: [...action.payload, ...fakeBooksList].map(item => {
     return {
      ...item,
      price: getRandomInt(1000),
      publishedDate: randomDate("02/13/2013", "01/01/2000")
     };
    })
   };
  case types.SET_LOADING:
   return {
    ...state,
    loading: action.payload
   };

  default:
   return state;
 }
};

export default cartReducer;

//EXTRA FUNCTIONS TO MOCK DATES AND PRICES
function getRandomInt(max) {
 return Math.floor(Math.random() * Math.floor(max));
}

function randomDate(d1, d2) {
 function randomValueBetween(min, max) {
  return Math.random() * (max - min) + min;
 }
 var date1 = d1 || "01-01-1970";
 var date2 = d2 || new Date().toLocaleDateString();
 date1 = new Date(date1).getTime();
 date2 = new Date(date2).getTime();
 if (date1 > date2) {
  return new Date(randomValueBetween(date2, date1)).toLocaleDateString();
 } else {
  return new Date(randomValueBetween(date1, date2)).toLocaleDateString();
 }
}

//A FAKE LIST OF BOOKS

const fakeBooksList = [
 {
  title: "LOTR - The Fellowship of the Ring",
  author: "J. R. R. Tolkien",
  link: "https://en.wikipedia.org/wiki/The_Fellowship_of_the_Ring"
 },
 {
  title: "LOTR - The Two Towers",
  author: "J. R. R. Tolkien",
  link: "https://en.wikipedia.org/wiki/The_Two_Towers"
 },
 {
  title: "LOTR - The Return of the King",
  author: "J. R. R. Tolkien",
  link: "https://en.wikipedia.org/wiki/The_Return_of_the_King"
 },
 {
  title: "TDT - The Gunslinger",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Dark_Tower:_The_Gunslinger"
 },
 {
  title: "The Drawing of the Three",
  author: "Stephen King",
  link:
   "https://en.wikipedia.org/wiki/The_Dark_Tower_II:_The_Drawing_of_the_Three"
 },
 {
  title: "The Waste Lands",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Dark_Tower_III:_The_Waste_Lands"
 },
 {
  title: "Wizard and Glass",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Dark_Tower_IV:_Wizard_and_Glass"
 },
 {
  title: "The Little Sisters of Eluria",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Little_Sisters_of_Eluria"
 },
 {
  title: "Wolves of the Calla",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Dark_Tower_V:_Wolves_of_the_Calla"
 },
 {
  title: "Song of Susannah",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Dark_Tower_VI:_Song_of_Susannah"
 },
 {
  title: "The Dark Tower",
  author: "Stephen King",
  link: "https://en.wikipedia.org/wiki/The_Dark_Tower_VII:_The_Dark_Tower"
 },
 {
  title: "The Wind Through the Keyho",
  author: "Stephen King",
  link:
   "https://en.wikipedia.org/wiki/The_Dark_Tower:_The_Wind_Through_the_Keyhole"
 }
];
