export const addItemToCart = (cartItems, cartItemToAdd) => {
 const existingCartItem = cartItems.find(
  cartItem => cartItem.title === cartItemToAdd.title
 );
 if (existingCartItem) {
  return cartItems.map(cartItem =>
   cartItem.title === cartItemToAdd.title
    ? { ...cartItem, quantity: cartItem.quantity + 1 }
    : cartItem
  );
 }
 return [...cartItems, { ...cartItemToAdd, quantity: 1 }];
};

export const removeItemFromCart = (cartItems, cartItemToRemove) => {
 const existingCartItem = cartItems.find(
  cartItem => cartItem.title === cartItemToRemove.title
 );

 if (existingCartItem.quantity === 1) {
  return cartItems.filter(
   cartItem => cartItem.title !== cartItemToRemove.title
  );
 }

 return cartItems.map(cartItem =>
  cartItem.title === cartItemToRemove.title
   ? { ...cartItem, quantity: cartItem.quantity - 1 }
   : cartItem
 );
};
