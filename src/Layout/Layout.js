import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import "../assets/scss/material-kit-react.scss";
import Modal from "../components/CartModal/CartModal";
import Header from "../components/Header/Header.js";
import Footer from "../components/Footer/Footer.js";
import HeaderLinks from "../components/Header/HeaderLinks.js";
import styles from "../assets/jss/material-kit-react/views/landingPage.js";
import classNames from "classnames";

const useStyles = makeStyles(styles);

export default props => {
 const classes = useStyles();
 return (
  <div>
   <Header
    color="transparent"
    brand="Books for sale"
    rightLinks={<HeaderLinks />}
    fixed
    changeColorOnScroll={{
     height: 80,
     color: "white"
    }}
   />
   <div className={classes.blackFrame} />
   <div className={classNames(classes.main, classes.mainRaised)}>
    <div className={classes.container}>{props.children}</div>
   </div>
   <Footer />
   <Modal />
  </div>
 );
};
